import myTaLib from "@lgj_sg/technical-analysis";

import Util from "bot-utils";

function getMacdResultForLookBackPeriod(macd, lookBackPeriod) {
  for (let i = 0; i < lookBackPeriod - 1; i++) {
    let macdLineValue = macd.macds[macd.macds.length - 1 - i];
    let signalLineValue = macd.signals[macd.signals.length - 1 - i];
    let prevMacdLineValue = macd.macds[macd.macds.length - 2 - i];
    let prevSignalLineValue = macd.signals[macd.signals.length - 2 - i];

    if (macdLineValue > signalLineValue && prevMacdLineValue < prevSignalLineValue) {
      return true;
    } else if (macdLineValue < signalLineValue && prevMacdLineValue > prevSignalLineValue) {
      return false;
    }
  }
  return null;
}

function getTrixResultForLookBackPeriod(trix, lookBackPeriod) {
  for (let i = 0; i < lookBackPeriod - 1; i++) {
    let rocValue = trix.roc[trix.roc.length - 1 - i];
    let signalLineValue = trix.signals[trix.signals.length - 1 - i];
    let prevRocValue = trix.roc[trix.roc.length - 2 - i];
    let prevSignalLineValue = trix.signals[trix.signals.length - 2 - i];

    if (rocValue < 0 && rocValue > signalLineValue && prevRocValue < prevSignalLineValue) {
      return true;
    } else if (rocValue > 0 && rocValue < signalLineValue && prevRocValue > prevSignalLineValue) {
      return false;
    }
  }
  return null;
}

function getRsiResultForLookBackPeriod(rsis, lookBackPeriod) {
  for (let i = 0; i < lookBackPeriod - 1; i++) {
    let rsi = rsis[rsis.length - 1 - i];

    if (rsi <= 30) {
      return true;
    } else if (rsi >= 100) {
      return false;
    }
  }
  return null;
}

function getBollingerBandsResultForLookBackPeriod(bollingerBands, lookBackPeriod, candles) {
  for (let i = 0; i < lookBackPeriod - 1; i++) {
    const upper = bollingerBands.upper[bollingerBands.upper.length - 1 - i];
    const lower = bollingerBands.lower[bollingerBands.lower.length - 1 - i];
    const closingPrice = candles[candles.length - 1 - i].closingPrice;

    if (closingPrice < lower) {
      return true;
    } else if (closingPrice < upper) {
      return false;
    }
  }
  return null;
}

function getCciResultForLookBackPeriod(ccis, lookBackPeriod) {
  for (let i = 0; i < lookBackPeriod - 1; i++) {
    let cci = ccis[ccis.length - 1 - i];
    let prevCci = ccis[ccis.length - 2 - i];

    if (cci <= -100 || (prevCci < 0 && cci > 0)) {
      return true;
    } else if (cci >= 100 || (prevCci > 0 && cci < 0)) {
      return false;
    }
  }
  return null;
}

function getSoResultForLookBackPeriod(so, lookBackPeriod) {
  for (let i = 0; i < lookBackPeriod - 1; i++) {
    let soD = so.dFulls[so.dFulls.length - 1 - i];
    let prevSoD = so.dFulls[so.dFulls.length - 2 - i];
    let soK = so.kFulls[so.kFulls.length - 1 - i];
    let prevSoK = so.kFulls[so.kFulls.length - 2 - i];

    if (soD <= 20 || (prevSoD > prevSoK && soD < soK && soD < 80)) {
      return true;
    } else if (soD >= 80 || (prevSoD < prevSoK && soD > soK && soD > 20)) {
      return false;
    }
  }
  return null;
}

function getRviResultForLookBackPeriod(rvis, lookBackPeriod) {
  for (let i = 0; i < lookBackPeriod - 1; i++) {
    let rvi = rvis.rvis[rvis.rvis.length - 1 - i];
    let prevRvi = rvis.rvis[rvis.rvis.length - 2 - i];
    let signal = rvis.signals[rvis.signals.length - 1 - i];
    let prevSignal = rvis.signals[rvis.signals.length - 2 - i];

    if (rvi < 0 && signal > rvi && prevSignal < prevRvi) {
      return true;
    } else if (rvi >= 0 && signal < rvi && prevSignal > prevRvi) {
      return false;
    }
  }
  return null;
}

function getMfiResultForLookBackPeriod(mfis, lookBackPeriod) {
  for (let i = 0; i < lookBackPeriod - 1; i++) {
    const mfi = mfis[mfis.length - 1 - i];
    if (mfi <= 20) {
      return true;
    } else if (mfi >= 80) {
      return false;
    }
  }
}

function tabulateTechnicalIndicatorsResult(assetPair, period, allCandles, indicatorsLookBackPeriod) {
  let results = analysisResults[assetPair][period];

  results.lastClosingPrice = allCandles[allCandles.length - 1].closingPrice;
  results.prevHighestPrice = allCandles[allCandles.length - 2].highestPrice;
  results.prevClosingPrice = allCandles[allCandles.length - 2].closingPrice;
  results.lastOpeningPrice = allCandles[allCandles.length - 1].openingPrice;

  results.buyHits = [];
  results.sellHits = [];

  // Stochastic Oscillator
  let so = results.so;
  let soResult = getSoResultForLookBackPeriod(so, indicatorsLookBackPeriod);
  if (soResult === true) {
    results.buyHits["SO"] = 1;
  } else if (soResult === false) {
    results.sellHits["SO"] = 1;
  }

  // Relative Strength Index
  let rsi = results.rsi;
  let rsiResult = getRsiResultForLookBackPeriod(rsi.rsis, indicatorsLookBackPeriod);
  if (rsiResult === true) {
    results.buyHits["RSI"] = 1;
  } else if (rsiResult === false) {
    results.sellHits["RSI"] = 1;
  }

  // // Triple EMA 
  let tema14 = results.tema[results.tema.length - 1];
  if (results.lastClosingPrice > results.lastOpeningPrice &&
    results.lastClosingPrice > tema14 && results.lastOpeningPrice > tema14) {
    results.buyHits["TEMA"] = 1;
  } else if (results.lastClosingPrice < results.lastOpeningPrice &&
    results.lastClosingPrice < tema14 && results.lastOpeningPrice < tema14) {
    results.sellHits["TEMA"] = 1;
  }

  // Triple EMA 50
  let tema50 = results.tema50[results.tema50.length - 1];
  if (results.lastClosingPrice > tema50) {
    results.buyHits["TEMA50"] = 1;
  }

  // Moving Average Convergence/Divergence
  let macd = results.macd;
  let macdResult = getMacdResultForLookBackPeriod(macd, indicatorsLookBackPeriod);
  if (macdResult === true) {
    results.buyHits["MACD"] = 1;
  } else if (macdResult === false) {
    results.sellHits["MACD"] = 1;
  }

  // Triple Exponential Average
  let trix = results.trix;
  let trixResult = getTrixResultForLookBackPeriod(trix, indicatorsLookBackPeriod);
  if (trixResult === true) {
    results.buyHits["TRIX"] = 1;
  } else if (trixResult === false) {
    results.sellHits["TRIX"] = 1;
  }

  // Awesome Oscillator
  let ao = results.ao.aos[results.ao.aos.length - 1];
  let prevAo = results.ao.aos[results.ao.aos.length - 2];
  if (prevAo < 0 && ao > 0) {
    results.buyHits["AO"] = 1;
  } else if (prevAo > 0 && ao < 0) {
    results.sellHits["AO"] = 1;
  }

  // Parabolic Stop and Reverse
  let psar = results.psar[results.psar.length - 1];
  let prevPsar = results.psar[results.psar.length - 2];
  if (prevPsar.trend === "bearish" && psar.trend === "bullish") {
    results.buyHits["PSAR"] = 1;
  } else if (prevPsar.trend === "bullish" && psar.trend === "bearish") {
    results.sellHits["PSAR"] = 1;
  }

  // Average Directional Movement Index
  let adx = results.adx;
  // if (adx >= 25), is trending
  if (results.adx.adx >= 20) {
    if (adx.plusDirectionIndicator14 > adx.minusDirectionIndicator14) {
      results.buyHits["ADX"] = 1;
    } else if (adx.plusDirectionIndicator14 < adx.minusDirectionIndicator14) {
      results.sellHits["ADX"] = 1;
    }
  }

  // Commodity Channel Index
  let cci = results.cci;
  let cciResult = getCciResultForLookBackPeriod(cci, indicatorsLookBackPeriod);
  if (cciResult === true) {
    results.buyHits["CCI"] = 1;
  } else if (cciResult === false) {
    results.sellHits["CCI"] = 1;
  }

  // Donchian Channel
  let dc = results.dc;
  if (results.lastClosingPrice > dc.upperChannel && results.lastClosingPrice > results.lastOpeningPrice) {
    results.buyHits["DC"] = 1;
  } else if (results.lastClosingPrice < dc.lowerChannel && results.lastOpeningPrice > results.lastClosingPrice) {
    results.sellHits["DC"] = 1;
  }

  // Simple Moving Averages
  let prevMa50 = results.prevMa50;
  let ma50 = results.ma50;
  let prevMa100 = results.prevMa100;
  let ma100 = results.ma100;
  let prevMa150 = results.prevMa150;
  let ma150 = results.ma150;
  if ((results.prevClosingPrice < prevMa50 && results.lastClosingPrice > ma50) ||
    (results.prevClosingPrice < prevMa100 && results.lastClosingPrice > ma100) ||
    (results.prevClosingPrice < prevMa150 && results.lastClosingPrice > ma150)) {
    results.buyHits["SMA"] = 1;
  } else if ((results.prevClosingPrice > prevMa50 && results.lastClosingPrice < ma50) ||
    (results.prevClosingPrice > prevMa100 && results.lastClosingPrice < ma100) ||
    (results.prevClosingPrice > prevMa150 && results.lastClosingPrice < ma150)) {
    results.sellHits["SMA"] = 1;
  }

  // Exponential Moving Averages
  let prevEma20 = results.prevEma20;
  let ema20 = results.ema20;
  let prevEma40 = results.prevEma40;
  let ema40 = results.ema40;
  if ((results.prevClosingPrice < prevEma20 && results.lastClosingPrice > ema20) ||
    (results.prevClosingPrice < prevEma40 && results.lastClosingPrice > ema40)) {
    results.buyHits["EMA"] = 1;
  } else if ((results.prevClosingPrice > prevEma20 && results.lastClosingPrice < ema20) ||
    (results.prevClosingPrice > prevEma40 && results.lastClosingPrice < ema40)) {
    results.sellHits["EMA"] = 1;
  }

  // Average True Range
  let atr = results.atr;
  if (results.lastClosingPrice <= ma50 + atr) {
    results.buyHits["ATR"] = 1;
  } else {
    results.sellHits["ATR"] = 1;
  }

  // Madrid Moving Average Ribbon
  let mmar = results.mmar;
  if (mmar === "lime" || mmar === "green") {
    results.buyHits["MMAR"] = 1;
  } else if (mmar === "maroon" || mmar === "red") {
    results.sellHits["MMAR"] = 1;
  }

  // Bollinger Bands
  let bollingerBands = results.bollinger;
  let bollingerResult = getBollingerBandsResultForLookBackPeriod(bollingerBands, indicatorsLookBackPeriod, allCandles);
  if (bollingerResult === true) {
    results.buyHits["BBANDS"] = 1;
  } else if (bollingerResult === false) {
    results.sellHits["BBANDS"] = 1;
  }

  // Force Index
  let fi = results.fi;
  if (fi[fi.length - 1] > 0) {
    results.buyHits["FI"] = 1;
  } else {
    results.sellHits["FI"] = 1;
  }

  // Money Flow Index
  let mfi = results.mfi;
  let mfiResult = getMfiResultForLookBackPeriod(mfi, indicatorsLookBackPeriod);
  if (mfiResult === true) {
    results.buyHits["MFI"] = 1;
  } else if (mfiResult === false) {
    results.sellHits["MFI"] = 1;
  }

  // Relative Vigor Index
  let rvi = results.rvi;
  let rviResult = getRviResultForLookBackPeriod(rvi, indicatorsLookBackPeriod);
  if (rviResult === true) {
    results.buyHits["RVI"] = 1;
  } else if (rviResult === false) {
    results.sellHits["RVI"] = 1;
  }

  // On-Balance Volume
  let obv = results.obv;
  let obvEma5 = results.obvEma5;
  let obvEma20 = results.obvEma20;
  if (obv[obv.length - 1] > obv[obv.length - 2] && results.lastClosingPrice < results.prevClosingPrice ||
    obvEma5 > obvEma20) {
    results.buyHits["OBV"] = 1;
  } else if (obv[obv.length - 1] < obv[obv.length - 2] && results.lastClosingPrice > results.prevClosingPrice ||
    obvEma5 < obvEma20) {
    results.sellHits["OBV"] = 1;
  }

  // Volume Weighted Average Price
  let lastVolumeWeightedAveragePrice = results.volumeWeightedAveragePrice;
  let prevVolumeWeightedAveragePrice = results.prevVolumeWeightedAveragePrice;
  if (results.lastClosingPrice > lastVolumeWeightedAveragePrice &&
    results.prevClosingPrice < prevVolumeWeightedAveragePrice) {
    results.buyHits["VWAP"] = 1;
  } else if (results.lastClosingPrice < lastVolumeWeightedAveragePrice &&
    results.prevClosingPrice > prevVolumeWeightedAveragePrice) {
    results.sellHits["VWAP"] = 1;
  }

  // Volume
  let lastVolume = results.lastVolume;
  let volEma20 = results.volumeEma20;
  if (results.lastClosingPrice > results.lastOpeningPrice && lastVolume > volEma20) {
    results.buyHits["Volume"] = 1;
  } else if (results.lastClosingPrice < results.lastOpeningPrice && lastVolume > volEma20) {
    results.sellHits["Volume"] = 1;
  }

  let buyHitsLength = Util.getLengthOfArray(results.buyHits);
  let sellHitsLength = Util.getLengthOfArray(results.sellHits);
  let index = buyHitsLength - sellHitsLength;

  return index;
}

function logTechnicalIndicatorsResults(assetPair, period) {
  const results = analysisResults[assetPair][period];

  console.log("======", "Analysis Results for " + assetPair.toUpperCase() + " (" + period + ")", "======");
  console.log("Stochastic Oscillator: " +
    results.so.kFulls[results.so.kFulls.length - 1] + " (K), " +
    results.so.dFulls[results.so.dFulls.length - 1] + " (D)");
  console.log("Relative Strength Index: " + results.rsi.rsis[results.rsi.rsis.length - 1]);
  console.log("Triple Exponential Moving Average: " +
    results.tema[results.tema.length - 1] + " (14), " +
    results.tema50[results.tema50.length - 1] + " (50)");
  console.log("Moving Average Convergence/Divergence: " +
    results.macd.macds[results.macd.macds.length - 1] + " (MACD), " +
    results.macd.signals[results.macd.signals.length - 1] + " (Signal)");

  console.log("Rate of Change: " + results.roc[results.roc.length - 1]);
  console.log("Triple Exponential Average: " +
    results.trix.roc[results.trix.roc.length - 1] + " (TRIX), " +
    results.trix.signals[results.trix.signals.length - 1] + " (Signal)");

  let ao = results.ao.aos[results.ao.aos.length - 1];
  let prevAo = results.ao.aos[results.ao.aos.length - 2];
  console.log("Awesome Oscillator: " + prevAo + " (Previous), " + ao + " (Current)");

  let psar = results.psar[results.psar.length - 1];
  let prevPsar = results.psar[results.psar.length - 2];
  console.log("Parabolic Stop and Reverse: " + psar.trend + " (Trend), " + prevPsar.psar + " (Previous), " +
    psar.psar + " (Current)");
  console.log("Average Directional Movement Index: " + results.adx.adx);
  console.log("Commodity Channel Index: " + results.cci[results.cci.length - 1]);
  console.log("Donchian Channel: " + results.dc.upperChannel + " (Upper), " + results.dc.lowerChannel + " (Lower)");
  console.log("Simple Moving Averages: " + results.ma50 + " (50), " +
    results.ma100 + " (100), " + results.ma150 + " (150), " + results.ma200 + " (200)");
  console.log("Exponential Moving Averages: " + results.ema20 + " (20), " + results.ema40 + " (40)");
  console.log("Average True Range: " + results.atr);
  console.log("Madrid Moving Average Ribbon: " + results.mmar);
  console.log("Bollinger Bands: " +
    results.bollinger.upper[results.bollinger.upper.length - 1] + " (upper), " +
    results.bollinger.middle[results.bollinger.middle.length - 1] + " (middle), " +
    results.bollinger.lower[results.bollinger.lower.length - 1] + " (lower)");
  console.log("Force Index: " + results.fi[results.fi.length - 1]);
  console.log("Money Flow Index: " + results.mfi[results.mfi.length - 1]);
  console.log("Relative Vigor Index: " + results.rvi.rvis[results.rvi.rvis.length - 1] + " (SMA10), " +
    results.rvi.signals[results.rvi.signals.length - 1] + " (VWMA4)");
  console.log("On-Balance Volume: " + results.obvEma5 + " (EMA5), " + results.obvEma20 + " (EMA20)");
  if (results.lastVolumeWeightedAveragePrice) {
    console.log("Volume Weighted Average Price: " + results.lastVolumeWeightedAveragePrice);
  }
  console.log("Volume: " + results.lastVolume + " (Last), " + results.volumeEma20 + " (EMA20)");
}

function logHits(assetPair, period) {
  if (!analysisResults[assetPair] || !analysisResults[assetPair][period]) {
    return;
  }

  const results = analysisResults[assetPair][period];

  console.log(assetPair, "Uptrend:", isAssetPairUptrend(results));

  let buyHitsLength = Util.getLengthOfArray(results.buyHits);
  if (buyHitsLength > 0) {
    let hits = assetPair + " Indicator Buy Hits: ";

    for (let hit in results.buyHits) {
      hits += hit + ", ";
    }
    hits = hits.slice(0, -2);
    console.log(hits);
  }

  let candlePatternBuyHitsLength = Util.getLengthOfArray(results.candlePatternBuyHits);

  if (candlePatternBuyHitsLength > 0) {
    let hits = assetPair + " Candlestick Pattern Buy Hits: ";

    for (let hit in results.candlePatternBuyHits) {
      hits += hit + ", ";
    }
    hits = hits.slice(0, -2);
    console.log(hits);
  }

  let sellHitsLength = Util.getLengthOfArray(results.sellHits);
  if (sellHitsLength > 0) {
    let hits = assetPair + " Indicator Sell Hits: ";

    for (let hit in results.sellHits) {
      hits += hit + ", ";
    }
    hits = hits.slice(0, -2);
    console.log(hits);
  }

  let candlePatternSellHitsLength = Util.getLengthOfArray(results.candlePatternSellHits);

  if (candlePatternSellHitsLength > 0) {
    let hits = assetPair + " Candlestick Pattern Sell Hits: ";

    for (let hit in results.candlePatternSellHits) {
      hits += hit + ", ";
    }
    hits = hits.slice(0, -2);
    console.log(hits);
  }

  let index = results.buySellIndex;
  console.log(assetPair.toUpperCase() + " Buy Sell Index" + " (" + period + ")" + ": " + index);
}

function isAssetPairUptrend(results) {
  const isUptrend = results.ema20 > results.ema40 && results.ma50 > results.ma150;

  return isUptrend;
}

function shouldBuy(currency, tradingPeriod, criteria) {
  if (!analysisResults[currency] || !analysisResults[currency][tradingPeriod]) {
    return false;
  }

  let results = analysisResults[currency][tradingPeriod];

  const buyHits = results.buyHits;
  const isUptrend = isAssetPairUptrend(results);
  
  const primary = isOneConditionMet(criteria.primary, buyHits);
  const confirmation = isOneConditionMet(criteria.confirmation, buyHits);
  const volume = isOneConditionMet(criteria.volume, buyHits);

  return isUptrend && primary && confirmation && volume; 
}

function isOneConditionMet(conditions, buyHits) {
  let met = false;
  if (conditions && conditions.length > 0) {
    for (const condition of conditions) {
      if (buyHits[condition] === 1) {
        met = true;
      }
    }
  } else {
    met = true;
  }

  return met;
}

function shouldSell(currency, tradingPeriod, criteria) {
  if (!analysisResults[currency] || !analysisResults[currency][tradingPeriod]) {
    return false;
  }

  let results = analysisResults[currency][tradingPeriod];

  const sellHits = results.sellHits;
  const isDowntrend = !isAssetPairUptrend(results);
  
  const primary = isOneConditionMet(criteria.primary, sellHits);
  const confirmation = isOneConditionMet(criteria.confirmation, sellHits);
  const volume = isOneConditionMet(criteria.volume, sellHits);

  return isDowntrend && primary && confirmation && volume; 
}

let analysisResults = [];

function performAnalysis(currency, tradingPeriod, allFullCandles, indicatorsLookBackPeriod) {
  return new Promise((resolve) => {

    if (!analysisResults[currency]) {
      analysisResults[currency] = [];
    }

    if (!analysisResults[currency][tradingPeriod]) {
      analysisResults[currency][tradingPeriod] = [];
    }

    performAnalysisForAssetPairAndPeriod(currency, tradingPeriod, allFullCandles, indicatorsLookBackPeriod);

    resolve();
  });
}

let trailingStops = {};

function setTrailingStop(value, currency, override) {
  if (!trailingStops[currency] || override === true) {
    trailingStops[currency] = value;
  } else {
    trailingStops[currency] = value > trailingStops[currency] ? value : trailingStops[currency];
  }
  return trailingStops[currency];
}

function performAnalysisForAssetPairAndPeriod(currency, period, allFullCandles, indicatorsLookBackPeriod) {
  if (allFullCandles.length < 150) {
    analysisResults[currency][period].buySellIndex = 0;
    return;
  }

  let last20Candles = Util.getLastXCandles(allFullCandles, 20);
  let last24Candles = Util.getLastXCandles(allFullCandles, 24);
  let last28Candles = Util.getLastXCandles(allFullCandles, 28);
  let last40Candles = Util.getLastXCandles(allFullCandles, 40);
  let last50Candles = Util.getLastXCandles(allFullCandles, 50);
  let last51Candles = Util.getLastXCandles(allFullCandles, 51);
  let last100Candles = Util.getLastXCandles(allFullCandles, 100);
  let last101Candles = Util.getLastXCandles(allFullCandles, 101);
  let last150Candles = Util.getLastXCandles(allFullCandles, 150);
  let last151Candles = Util.getLastXCandles(allFullCandles, 151);
  let last201Candles = Util.getLastXCandles(allFullCandles, 201);
  let last250Candles = Util.getLastXCandles(allFullCandles, 250);

  // Stochastic Oscillator
  analysisResults[currency][period].so = myTaLib.calculateStochasticOscillator(last24Candles,
    14, 3, 3, analysisResults[currency][period].so);

  // Relative Strength Index
  analysisResults[currency][period].rsi = myTaLib.calculateRelativeStrengthIndex(last24Candles,
    14, analysisResults[currency][period].rsi);

  // Moving Average Convergence/Divergence
  analysisResults[currency][period].macd = myTaLib.getMovingAverageConvergenceDivergenceSignal(last100Candles,
    12, 26, 9, "closingPrice", analysisResults[currency][period].macd);

  // Awesome Oscillator
  analysisResults[currency][period].ao = myTaLib.calculateAwesomeOscillator(last40Candles,
    analysisResults[currency][period].ao);

  // Triple Exponential Moving Average (14)
  analysisResults[currency][period].tema = myTaLib.calculateTripleExponentialMovingAverage(
    last40Candles, 14, "closingPrice");

  // Triple Exponential Moving Average (50)
  analysisResults[currency][period].tema50 = myTaLib.calculateTripleExponentialMovingAverage(
    last250Candles, 50, "closingPrice");

  // Parabolic Stop and Reverse
  analysisResults[currency][period].psar = myTaLib.calculateParabolicStopAndReverse(last24Candles, 0.02, 0.02, 0.2,
    analysisResults[currency][period].psar);
  let psar = analysisResults[currency][period].psar[analysisResults[currency][period].psar.length - 1];
  if (psar.trend === "bullish") {
    setTrailingStop(psar.psar, currency);
  }

  // Average True Range
  analysisResults[currency][period].atr = myTaLib.calculateAverageTrueRange(last150Candles, 14);

  // Average Directional Movement Index
  analysisResults[currency][period].adx = myTaLib.calculateAverageDirectionalMovementIndex(last28Candles);

  // Commodity Channel Index
  analysisResults[currency][period].cci = myTaLib.calculateCommodityChannelIndex(last28Candles, 20);

  // Donchian Channel
  analysisResults[currency][period].dc = myTaLib.calculateDonchianChannel(last100Candles);

  // Simple Moving Averages (50)
  const sma50 = myTaLib.calculateSimpleMovingAverage(last51Candles, 50, "closingPrice");
  analysisResults[currency][period].ma50 = sma50[sma50.length - 1];
  analysisResults[currency][period].prevMa50 = sma50[sma50.length - 2];

  // Simple Moving Averages (100)
  const sma100 = myTaLib.calculateSimpleMovingAverage(last101Candles, 100, "closingPrice");
  analysisResults[currency][period].ma100 = sma100[sma100.length - 1];
  analysisResults[currency][period].prevMa100 = sma100[sma100.length - 2];

  // Simple Moving Averages (150)
  const sma150 = myTaLib.calculateSimpleMovingAverage(last151Candles, 150, "closingPrice");
  analysisResults[currency][period].ma150 = sma150[sma150.length - 1];
  analysisResults[currency][period].prevMa150 = sma150[sma150.length - 2];

  // Simple Moving Averages (200)
  const sma200 = myTaLib.calculateSimpleMovingAverage(last201Candles, 200, "closingPrice");
  analysisResults[currency][period].ma200 = sma200[sma200.length - 1];
  analysisResults[currency][period].prevMa200 = sma200[sma200.length - 2];

  // Exponential Moving Average (6)
  let ema6 = myTaLib.calculateExponentialMovingAverage(last20Candles, 6, "closingPrice");
  analysisResults[currency][period].ema6 = ema6[ema6.length - 1];
  analysisResults[currency][period].prevEma6 = ema6[ema6.length - 2];

  // Exponential Moving Average (20)
  let ema20 = myTaLib.calculateExponentialMovingAverage(last40Candles, 20, "closingPrice");
  analysisResults[currency][period].ema20 = ema20[ema20.length - 1];
  analysisResults[currency][period].prevEma20 = ema20[ema20.length - 2];

  // Exponential Moving Average (40)
  let ema40 = myTaLib.calculateExponentialMovingAverage(last100Candles, 40, "closingPrice");
  analysisResults[currency][period].ema40 = ema40[ema40.length - 1];
  analysisResults[currency][period].prevEma40 = ema40[ema40.length - 2];

  // Madrid Moving Average Ribbon
  analysisResults[currency][period].mmar = myTaLib.calculateMadridMovingAverageRibbon(last250Candles, "closingPrice");

  // Bollinger Bands
  analysisResults[currency][period].bollinger = myTaLib.calculateBollingerBands(last28Candles, 20, "closingPrice");

  // Force Index
  analysisResults[currency][period].fi = myTaLib.calculateForceIndex(last20Candles, 13);

  // Money Flow Index
  analysisResults[currency][period].mfi = myTaLib.calculateMoneyFlowIndex(last20Candles, 14);

  // Relative Vigor Index
  analysisResults[currency][period].rvi = myTaLib.calculateRelativeVigorIndex(last20Candles, 10);

  // On-Balance Volume
  analysisResults[currency][period].obv = myTaLib.calculateOnBalanceVolume(last50Candles);
  let obvEma5 = myTaLib.calculateExponentialMovingAverage(
    analysisResults[currency][period].obv.slice(-15), 5);
  analysisResults[currency][period].obvEma5 = obvEma5[obvEma5.length - 1];
  let obvEma20 = myTaLib.calculateExponentialMovingAverage(
    analysisResults[currency][period].obv.slice(-40), 20);
  analysisResults[currency][period].obvEma20 = obvEma20[obvEma20.length - 1];

  // Volume Weighted Average Price
  analysisResults[currency][period].lastVolumeWeightedAveragePrice =
    allFullCandles[allFullCandles.length - 1].volumeWeightedAveragePrice;
  analysisResults[currency][period].prevVolumeWeightedAveragePrice =
    allFullCandles[allFullCandles.length - 2].volumeWeightedAveragePrice;

  // Volume
  analysisResults[currency][period].lastVolume = allFullCandles[allFullCandles.length - 1].volume;
  let volumeEma20 = myTaLib.calculateExponentialMovingAverage(last40Candles, 20, "volume");
  analysisResults[currency][period].volumeEma20 = volumeEma20[volumeEma20.length - 1];

  // Triple Exponential Average
  analysisResults[currency][period].trix = myTaLib.calculateTripleExponentialAverage(
    last250Candles, 15, 9, "closingPrice");

  // Rate of Change
  analysisResults[currency][period].roc = myTaLib.calculateRateOfChange(last20Candles, 9, "closingPrice");

  let technicalIndicatorsResult = tabulateTechnicalIndicatorsResult(currency, period, allFullCandles,
    indicatorsLookBackPeriod);
  let candlestickPatternsResult = tabulateCandlestickPatternsResult(currency, period, allFullCandles);

  analysisResults[currency][period].buySellIndex = technicalIndicatorsResult + candlestickPatternsResult;
}

function tabulateCandlestickPatternsResult(currency, period, allCandles) {
  let lastCandle = allCandles[allCandles.length - 1];
  let last2Candles = Util.getLastXCandles(allCandles, 2);
  let last3Candles = Util.getLastXCandles(allCandles, 3);

  let results = analysisResults[currency][period];
  let buyIndicatorHits = results.buyHits;
  let sellIndicatorHits = results.sellHits;

  analysisResults[currency][period].candlePatternBuyHits = [];
  analysisResults[currency][period].candlePatternSellHits = [];

  if (buyIndicatorHits["SO"] === 1 && results.ema20 > results.ema40 && results.ma50 > results.ma150) {
    if (last2Candles[0].lowestPrice <= results.ma50) {
      // Bullish Pin
      if (myTaLib.isBullishPin(lastCandle)) {
        analysisResults[currency][period].candlePatternBuyHits["Bullish Pin"] = 1;
      }

      // Bullish Tweezer Bottoms
      if (myTaLib.isBullishTweezerBottoms(last2Candles)) {
        analysisResults[currency][period].candlePatternBuyHits["Bullish Tweezer Bottoms"] = 1;
      }

      // One White Soldier
      if (myTaLib.isOneWhiteSoldier(last2Candles)) {
        analysisResults[currency][period].candlePatternBuyHits["One White Soldier"] = 1;
      }

      // Morning Star
      if (myTaLib.isMorningStar(last3Candles)) {
        analysisResults[currency][period].candlePatternBuyHits["Morning Star"] = 1;
      }

      // Bullish Engulfing
      if (myTaLib.isBullishEngulfing(last2Candles)) {
        analysisResults[currency][period].candlePatternBuyHits["Bullish Engulfing"] = 1;
      }
    }
  }

  if (sellIndicatorHits["SO"] === 1 && results.ema20 < results.ema40 && results.ma50 < results.ma150) {
    if (last2Candles[0].highestPrice >= results.ma50) {
      // Bearish Pin
      if (myTaLib.isBearishPin(lastCandle)) {
        analysisResults[currency][period].candlePatternSellHits["Bearish Pin"] = 1;
      }

      // Bearish Tweezer Bottoms
      if (myTaLib.isBearishTweezerBottoms(last2Candles)) {
        analysisResults[currency][period].candlePatternSellHits["Bearish Tweezer Bottoms"] = 1;
      }

      // One Black Crow
      if (myTaLib.isOneBlackCrow(last2Candles)) {
        analysisResults[currency][period].candlePatternSellHits["One Black Crow"] = 1;
      }

      // Evening Star
      if (myTaLib.isEveningStar(last3Candles)) {
        analysisResults[currency][period].candlePatternSellHits["Evening Star"] = 1;
      }

      // Bearish Engulfing
      if (myTaLib.isBearishEngulfing(last2Candles)) {
        analysisResults[currency][period].candlePatternSellHits["Bearish Engulfing"] = 1;
      }
    }
  }

  let buyHitsLength = Util.getLengthOfArray(analysisResults[currency][period].candlePatternBuyHits);
  let sellHitsLength = Util.getLengthOfArray(analysisResults[currency][period].candlePatternSellHits);

  let index = buyHitsLength - sellHitsLength;

  return index;
}

module.exports = {
  shouldBuy,
  shouldSell,
  performAnalysis,
  setTrailingStop,
  logHits,
  logTechnicalIndicatorsResults,
};